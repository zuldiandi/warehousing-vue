<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    public function getAllProduct()
    {
        $product = Product::get();
        return Datatables::of($product)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = '<a href="#" class="edit btn btn-success btn-sm">Cek BOM</a>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
