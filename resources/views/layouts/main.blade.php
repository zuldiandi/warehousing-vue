<html>

<head>
    <title>Warehousing Vue</title>
    <link rel="stylesheet" href="{{asset('module/css/index.css')}}" />
</head>

<body>
    <div id="app">
        <main-layout></main-layout>
    </div>
    <script src="{{asset('module/js/app.js')}}"></script>
</body>

</html>