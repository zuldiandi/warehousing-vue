import Vue from 'vue';
import router from './vue/routes/index';

import vueAxios from './vue/plugins/axios';
window.$ = require('jquery');
require('datatables.net-bs5');
// require('datatables.net-responsive-bs5');


import mainLayout from './vue/wrapper.vue';

Vue.prototype.$vueAxios = vueAxios;

var app = new Vue({
    el: '#app',
    router,
    components: {
        mainLayout
    }
});