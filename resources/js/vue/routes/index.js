import Vue from 'vue';
import VueRouter from 'vue-router';

import product from './product';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        ...product
    ]
});

export default router;