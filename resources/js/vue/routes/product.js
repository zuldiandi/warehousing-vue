const home = () => import('../views/product/index');
const detail = () => import('../views/product/detail');

export default [{
    path: '/',
    component: home,
    name: 'product',

},
{
    path: '/product/detail',
    component: detail,
    name: 'product.detail'
}];