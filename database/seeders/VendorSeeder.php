<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private array $data = [
        ['001', 'Vendor A','09081'],
        ['002', 'Vendor B','09082'],
        ['003', 'Vendor C','09083']
    ];
    
    public function run()
    {
        $length = count($this->data);
        for ($i = 0; $i < $length; $i++) {
            DB::table('vendors')->insert([
                'vendor_code' => $this->data[$i][0],
                'vendor_name' => $this->data[$i][1],
                'vendor_contact' => $this->data[$i][2]
            ]);
        }
    }
}
