<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private array $data = [
        [10000, 1],
        [20000, 2],
        [30000, 1]
    ];
    
    public function run()
    {
        $length = count($this->data);
        for ($i = 0; $i < $length; $i++) {
            DB::table('prices')->insert([
                'value' => $this->data[$i][0],
                'price_unit' => $this->data[$i][1]
            ]);
        }
    }
}
