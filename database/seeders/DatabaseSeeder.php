<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PriceUnitSeeder::class,
            PartSeeder::class,
            PriceSeeder::class,
            ProductSeeder::class,
            VendorSeeder::class
        ]);
    }
}
