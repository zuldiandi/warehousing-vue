<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private array $data = [
        ['IDR'],
        ['JPY'],
    ];
    
    public function run()
    {
        $length = count($this->data);
        for ($i = 0; $i < $length; $i++) {
            DB::table('price_units')->insert([
                'price_unit' => $this->data[$i][0]
            ]);
        }
    }
}
