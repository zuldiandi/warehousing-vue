<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private array $data = [
        ['001', 'produk A'],
        ['002', 'produk B'],
        ['003', 'produk C']
    ];

    public function run()
    {
        $length = count($this->data);
        for ($i = 0; $i < $length; $i++) {
            DB::table('products')->insert([
                'product_code' => $this->data[$i][0],
                'product_name' => $this->data[$i][1]
            ]);
        }
    }
}
