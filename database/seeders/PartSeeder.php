<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private array $data = [
        ['001', 'Part A'],
        ['002', 'Part B'],
        ['003', 'Part C']
    ];
    
    public function run()
    {
        $length = count($this->data);
        for ($i = 0; $i < $length; $i++) {
            DB::table('parts')->insert([
                'part_code' => $this->data[$i][0],
                'part_name' => $this->data[$i][1]
            ]);
        }
    }
}
